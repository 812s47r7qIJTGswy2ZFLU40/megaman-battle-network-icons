![Title](gitlab/icons-showcase.png)

**Download**:
Click on **Code** button and download as zip if you want. 

Was surprised that no one has done this or made it public. So I'm sharing with anyone interested in adding them to their homescreen. 

The icons were ripped from the game by [Sprites Inc](https://www.sprites-inc.co.uk/sprite.php?local=/EXE/) & [The Spriters Resources](https://www.spriters-resource.com/). All I did was rescale the icons and modified the colors.

The Icons come in 5 sizes:
- 48x48
- 96x96
- 192x192
- 512x512
- 1024x1024

I've also included a couple of folders from mmbn3.

I recommend a launcher that allows you to use your gallery to set app icons like Nova Launcher.

## Icons

![Icons MMBN1](gitlab/icon-ref-1.png)
![Icons MMBN2](gitlab/icon-ref-2.png)
![Icons MMBN3](gitlab/icon-ref-3.png)
![Icons MMBN4](gitlab/icon-ref-4.png)
![Icons MMBN5](gitlab/icon-ref-5.png)
![Icons MMBN6](gitlab/icon-ref-6.png)

## Screenshot
![Screenshot of Phone](gitlab/screenshot.png)